C.UserPostContainer = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData: function() {
        var currentUser = Meteor.userId();
        var subPosts = Meteor.subscribe('userPosts', currentUser);
        var subUserServices = Meteor.subscribe('userData');
        var postFinder = Posts.find({
            author: currentUser
        }).fetch();
        return {
            feedReady: subPosts.ready(),
            userServicesReady: subUserServices.ready(),
            posts: _.sortBy(postFinder, "updatedAt").reverse(),
            currentUser: Meteor.userId()
        };
    },
    render() {
        if (!this.data.feedReady) {
            return <div></div>
        }
        else if (this.data.posts === undefined || this.data.posts.length == 0) {
            return (
                <div className="container text-center full-height-wheader v-center">
                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <div className="row">
                            <p>You have no posts!</p>
                            <p><a href="/create" key="inline-link">Create a post</a></p>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            var tableData = _.map(this.data.posts, function(doc) {
                let titleLink = <a href={FlowRouter.path("singlePostbyId", {_id: doc._id})}>{ doc.title }</a>
                let date = moment(doc.createdAt).fromNow();
                let timestamp = moment(doc.createdAt);
                let isPublished;
                if (doc.published) {
                    isPublished = "Published";
                }
                else {
                    isPublished = "Draft";
                }
                return {
                    "Title": titleLink,
                    "Last Updated": date,
                    "Timestamp": timestamp,
                    "Status": isPublished
                };
            })
            var tableDataSorted = _.sortBy(tableData, "Last Updated");
            return (
                <div className="container">
                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <C.Table data={ tableData } />
                    </div>
                </div>
            );
        }
    }
});

C.Table = React.createClass({
    propTypes: {
        data: React.PropTypes.array.isRequired
    },
    render() {
        var data = _.sortBy(this.props.data, "Timestamp").reverse();
        return (
            <div>
                <Table className="table" data={ data } columns={['Title', 'Last Updated', 'Status']} />
            </div>
        );
    }
});

C.TableRow = React.createClass({
    propTypes: {
        cells: React.PropTypes.object.isRequired
    },
    render() {
        var cells = _.values(this.props.cells);
        return (
            <div>
                <tr>
                    {
                        _.map(cells, function (doc, index) {
                            return <td key={index}>{doc}</td>
                        })
                    }
                </tr>
            </div>
        );
    }
});
