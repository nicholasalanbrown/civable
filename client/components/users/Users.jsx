C.UserLogin = React.createClass({
    loginWithFacebook() {
      Meteor.loginWithFacebook({}, function (error, response) {
        if (error) {
            console.log(error);
        }
        else {
            FlowRouter.go('home');
        }
      });
    },
    render() {
        return (
            <div className="container full-height v-center text-center">
                <C.FacebookLogin />
            </div>
        )
    }
})

C.UserSignup = React.createClass({
    render() {
        return (
            <div>
                <C.PasswordSignup />
            </div>
        )
    }
})

C.PasswordLogin = React.createClass({
    login: function(e) {
        e.preventDefault();
        var email = React.findDOMNode(this.refs.email).value.trim();
        var password = React.findDOMNode(this.refs.password).value.trim();
        if (!email || !password) {
            return;
        }
        Meteor.loginWithPassword(email, password);
        React.findDOMNode(this.refs.email).value = '';
        React.findDOMNode(this.refs.password).value = '';
    },
    render() {
        return (
            <div className="container">
                <form className="commentForm" onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <input className="form-control input-lg" type="email" ref="email" placeholder="Email" />
                  </div>
                  <div className="form-group">
                    <input className="form-control input-lg" type="password" ref="password" placeholder="Password" />
                  </div>
                <button onClick={this.login}>Login</button>
                </form>
            </div>
        )
    }
})

C.PasswordSignup = React.createClass({
    signup: function(e) {
        e.preventDefault();
        var name = React.findDOMNode(this.refs.name).value.trim();
        var email = React.findDOMNode(this.refs.email).value.trim();
        var password = React.findDOMNode(this.refs.password).value.trim();
        if (!email || !password) {
            return;
        }
        Accounts.createUser({
            email: email,
            password: password,
            profile: {name: name}
        }, function (error, response) {
            if (error) {
                console.log(error.reason);
            }
            else {
                FlowRouter.go('posts');
            }
        })
        React.findDOMNode(this.refs.name).value = '';
        React.findDOMNode(this.refs.email).value = '';
        React.findDOMNode(this.refs.password).value = '';
    },
    render() {
        return (
            <div className="container">
                <form className="signup-form" onSubmit={this.signup}>
                  <div className="form-group">
                    <input className="form-control input-lg" type="text" ref="name" placeholder="Full Name" />
                  </div>
                  <div className="form-group">
                    <input className="form-control input-lg" type="email" ref="email" placeholder="Email" />
                  </div>
                  <div className="form-group">
                    <input className="form-control input-lg" type="password" ref="password" placeholder="Password" />
                  </div>
                <button onClick={this.signup}>Sign Up</button>
                </form>
            </div>
        )
    }
})

C.FacebookLogin = React.createClass({
    loginWithFacebook: function () {
      Meteor.loginWithFacebook({}, function (error, response) {
        if (error) {
            console.log(error);
        }
        else {
            FlowRouter.go('myPosts');
        }
      });
    },
    render() {
        return (
            <div>
                <button onClick={this.loginWithFacebook} className="btn" id="btnLoginFacebook">
                    Login with Facebook
                </button>
            </div>
        )
    }
})

