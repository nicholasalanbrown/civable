C.UserAvatar = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData() {
        return {

        }
    },
    render() {
        return (
            <div className="comment-picture">
                <img src={this.props.imageSource} />
            </div>
        )
    }
});
