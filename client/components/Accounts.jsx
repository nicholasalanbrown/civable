SignInBox = React.createClass({
  signIn: function(e) {
    e.preventDefault();
    var email = React.findDOMNode(this.refs.email).value.trim();
    var password = React.findDOMNode(this.refs.password).value.trim();
    if (!email || !password) {
      return;
    }
    var email = email;
    var password = password;
    Meteor.loginWithPassword(email, password, function(err) {
        if (err) {
            console.log(err);
        } else {
            FlowRouter.go("/");
        }
    });
  },
  render: function() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-4 col-lg-offset-4">
                    <form className="form-signin mg-btm">
                        <h3 className="heading-desc">`
            <button type="button" className="close pull-right" aria-hidden="true">×</button>
            Sign In to Civable</h3>
                        <div className="social-box">
                            <div className="row mg-btm">
                                <div className="col-md-12">
                                    <a href="#" className="btn btn-primary btn-block">
                                        <i className="icon-facebook"></i>    Login with Facebook
                                    </a>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <a href="#" className="btn btn-info btn-block">
                                        <i className="icon-twitter"></i>    Login with Twitter
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="main">
                            <input ref="email" type="email" className="form-control input-lg" placeholder="Email" autofocus />
                            <input ref="password" type="password" className="form-control input-lg" placeholder="Password" />
                            <span className="clearfix"></span>
                        </div>
                        <div className="login-footer">
                            <div className="row">
                                <div className="col-xs-6 col-md-6 pull-right">
                                    <button type="submit" onClick={this.signIn} className="btn btn-large btn-danger pull-right">Sign In</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
  }
});

SignUpBox = React.createClass({
  register: function(e) {
    e.preventDefault();
    var email = React.findDOMNode(this.refs.email).value.trim();
    var password = React.findDOMNode(this.refs.password).value.trim();
    if (!email || !password) {
      return;
    }
    var doc = {
    email: email,
    password: password,
    profile: {
        createdAt: new Date(TimeSync.serverTime()),
        updatedAt: new Date(TimeSync.serverTime()),
    }
    };
    Accounts.createUser(doc, function(err) {
        if (err) {
            throw new Meteor.Error(err);
        } else {
            FlowRouter.go('home');

        }
    });
  },
  render: function() {
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-4 col-lg-offset-4">
                    <form className="form-signin mg-btm">
                        <h3 className="heading-desc">`
            <button type="button" className="close pull-right" aria-hidden="true">×</button>
            Sign Up for Civable</h3>
                        <div className="social-box">
                            <div className="row mg-btm">
                                <div className="col-md-12">
                                    <a href="#" className="btn btn-primary btn-block">
                                        <i className="icon-facebook"></i>    Login with Facebook
                                    </a>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <a href="#" className="btn btn-info btn-block">
                                        <i className="icon-twitter"></i>    Login with Twitter
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="main">
                            <input ref="email" type="email" className="form-control input-lg" placeholder="Email" autofocus />
                            <input ref="password" type="password" className="form-control input-lg" placeholder="Password" />
                            <span className="clearfix"></span>
                        </div>
                        <div className="login-footer">
                            <div className="row">
                                <div className="col-xs-6 col-md-6 pull-right">
                                    <button type="submit" onClick={this.register} className="btn btn-large btn-danger pull-right">Signup</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
  }
});