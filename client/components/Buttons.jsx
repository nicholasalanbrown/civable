Button = React.createClass({
  	render() {
  		var classes = "btn btn-primary" + " " + this.props.size;
    	return <a className={classes} >{this.props.text}</a>
  }
});