CommentBox = React.createClass({
    render: function() {
        return (
            <div>
        <h2>CommentBox</h2>
        <CommentList comments={this.props.comments} />
        <div className="commentForm">
          <CommentForm />
        </div>
        </div>
        );
    }
});

C.CommentList = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData() {
        var subComments = Meteor.subscribe("comments", this.props.postId);
        var subUpvotes = Meteor.subscribe('userUpvotes', this.props.postId, 'comment');
        var commentFinder = Comments.find({}, {
            sort: {
                upvotes: -1,
                createdAt: -1
            }
        }).fetch();
        var upvoteFinder = Upvotes.find({}).fetch();
        return {
            currentUser: Meteor.userId(),
            commentsReady: subComments.ready(),
            comments: commentFinder,
            upvote: upvoteFinder
        };
    },
    render() {
        let self = this;
        if (this.data.commentsReady) {
            var commentNodes = _.map(this.data.comments, function(doc, index) {
                let upvoteDoc = Upvotes.findOne({
                    postId: self.props.postId,
                    type: 'comment',
                    commentId: doc._id,
                    user: self.data.currentUser
                });
                let upvoted;
                if (upvoteDoc) {
                    upvoted = true;
                } else {
                    upvoted = false;
                }
                return (<C.CommentItem postId={ self.props.postId } commentId={doc._id} commenterName={doc.commenterName} commenterPicture={doc.commenterPicture} createdAt={doc.createdAt} currentUser={self.data.currentUser} upvoted= { upvoted } upvotes={doc.upvotes} key={index}>
          {doc.text}
        </C.CommentItem>)
            })
            return (
                <div>
          <ul className="comment-list">
            {commentNodes}
          </ul>
        </div>
            );
        } else {
            return <div></div>;
        }
    }
});

C.CommentItem = React.createClass({
    propTypes: {
        postId: React.PropTypes.string.isRequired,
        commentId: React.PropTypes.string.isRequired,
        commenterName: React.PropTypes.string.isRequired,
        commenterPicture: React.PropTypes.string.isRequired,
        createdAt: React.PropTypes.string.isRequired,
        currentUser: React.PropTypes.string,
        upvoted: React.PropTypes.bool.isRequired,
        upvotes: React.PropTypes.number.isRequired
    },
    upvote: function() {
        if (!this.props.currentUser) {
            FlowRouter.go('login');
        } else if (this.props.upvoted) {
            Meteor.call('upvoteComment', this.props.postId, this.props.commentId, true);
        } else {
            Meteor.call('upvoteComment', this.props.postId, this.props.commentId);
        }
    },
    render: function() {
        var date = moment(this.props.createdAt).fromNow();
        return (
            <div className="comment-box">
        <div className="comment-list">
        <li>
        <div className="comment-item">
            <div className="comment-content">
              <div className="comment-picture">
                <img src={ this.props.commenterPicture } />
              </div>
              <div className="comment-meta">
                  <p>{ this.props.commenterName }</p> <span className="date sub-text">{ date }</span>
              </div>
              <div className="comment-text">
                  <p>{ this.props.children }</p>
              </div>
            </div>
            <C.Upvote shape= { "triangle" } upvotes= {this.props.upvotes } callbackParent={this.upvote} upvoted={ this.props.upvoted } />
          </div>
          </li>
        </div>
      </div>
        );
    }
});


C.CommentForm = React.createClass({
    propTypes: {
        postId: React.PropTypes.string.isRequired,
        currentUserId: React.PropTypes.string.isRequired,
        currentUserName: React.PropTypes.string.isRequired,
        currentUserPicture: React.PropTypes.string.isRequired,
    },
    handleSubmit: function(e) {
        e.preventDefault();
        const commenter = this.props.currentUserId,
            commenterName = this.props.currentUserName,
            commenterPicture = this.props.currentUserPicture;
        const postId = this.props.postId;
        var text = React.findDOMNode(this.refs.text).value.trim();
        if (!text || !commenter) {
            return;
        }
        Meteor.call('insertComment', postId, commenter, commenterName, commenterPicture, text, function(error, result) {
            if (error) {
                console.log(error.reason);
            } else {
                return result;
            }
        });
        React.findDOMNode(this.refs.text).value = '';
        return;
    },
    render: function() {
        return (
            <div className="comment-form">
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <textarea className="form-control" type="textarea" cols="50" ref="text" placeholder="Say something..." />
          </div>
          <a className="btn btn-lg btn-primary pull-right" onClick={this.handleSubmit}>Post</a>
        </form>
      </div>
        );
    }
});

C.CommentLoggedOut = React.createClass({
    render() {
        return (
            <div>
        You must be logged in to comment. <a href={FlowRouter.path("login")}>Login</a> or <a href={FlowRouter.path("signup")}>sign up.</a>
      </div>
        );
    }
});
