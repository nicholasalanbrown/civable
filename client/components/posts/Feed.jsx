C.FeedContainer = React.createClass({
    mixins: [ReactMeteorData],
    getInitialState() {
        return {
            recordCount: {
                posts: 10
                    //postComments: 5 XXX no comment limit
            },

            // TODO have this component grab children's needed fields
            // from their statics object
            fieldsNeeded: {
                posts: {
                    _id: true,
                    desc: true,
                    likeCount: true,
                    commentCount: true,
                    userName: true,
                    createdAt: true,
                    ownerId: true,
                },
                postComments: {
                    _id: true,
                    createdAt: true,
                    username: true,
                    desc: true,
                    postId: true,
                }
            }
        };
    },
    // re-renders view if any reactive data source changes. `sub` is reactive
    // and will change when any new data is availible from subscription.
    getMeteorData() {
        var recordCount = this.state.recordCount;
        var subFirstPosts = Meteor.subscribe('firstPosts', 10, true);
        var subPosts = Meteor.subscribe('posts', recordCount, true);
        var subUpvotes = Meteor.subscribe('feedUpvotes', 'post');
        var subUserServices = Meteor.subscribe('userData');
        var subCategories = Meteor.subscribe('categories');
        var postFinder = Posts.find({}, {
            sort: {
                upvotes: -1
            }
        }).fetch();
        var upvoteFinder = Upvotes.find().fetch();
        var categoryFinder = Categories.find().fetch();
        return {
            firstFeedReady: subFirstPosts.ready(),
            upvotesReady: subUpvotes.ready(),
            feedReady: subPosts.ready(),
            userServicesReady: subUserServices.ready(),
            categoriesReady: subCategories.ready(),
            posts: postFinder,
            userUpvotes: upvoteFinder,
            categories: categoryFinder,
            currentUser: Meteor.userId()
        };
    },

    // pass this down to children so they can increase the limit when needed
    incrementLimit() {
        var limits = _.extend({}, this.state.recordCount);
        limits.posts = limits.posts + 10;

        this.setState({
            recordCount: limits
        });
        return this.state;
    },
    handleScroll: function() {
        var incrementLimit = this.props.incrementLimit;
        var self = this;
        $(window).scroll(function() {
            if (self.data.feedReady) {
                if ($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
                    self.incrementLimit();
                }
            }
        });
    },
    componentDidMount: function() {
        window.addEventListener('scroll', this.handleScroll);
    },
    componentWillUnmount: function() {
        window.removeEventListener('scroll', this.handleScroll);
    },
    render() {
        var loading;
        if (!this.data.firstFeedReady || !this.data.upvotesReady) {
            return <C.LoadingSpinner />
        } else {
            if (!this.data.feedReady) {
                loading = <C.LoadingSpinner />
            }
            return (
                <div>
                    <C.SubNav />
                    <div className="container feed-container">
                        <C.FeedList incrementLimit={this.incrementLimit} posts={this.data.posts} userUpvotes={this.data.userUpvotes} currentUser={this.data.currentUser} />
                        {loading}
                    </div>
                </div>
            );
        }
    }
});

C.FeedList = React.createClass({
    propTypes: {
        posts: React.PropTypes.array.isRequired,
        currentUser: React.PropTypes.string
    },
    render() {
        return (
            <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 feed-list">
                <ul>
                    {
                      this.props.posts.map(doc => {
                        let upvoteDoc = Upvotes.findOne({postId: doc._id, user: this.props.currentUser});
                        let upvoted;
                        if (upvoteDoc) {
                            upvoted = true
                        }
                        else {
                            upvoted = false;
                        }
                        return <C.FeedItem key={doc._id}
                          { ...doc }
                          destroyPost={ doc.destroy } currentUser={this.props.currentUser} upvoted={ upvoted } />
                      })
                    }
                </ul>
            </div>
        );
    }
});

C.FeedItem = React.createClass({
    propTypes: {
        title: React.PropTypes.string.isRequired,
        summary: React.PropTypes.string.isRequired,
        author: React.PropTypes.string.isRequired,
        authorName: React.PropTypes.string.isRequired,
        authorPicture: React.PropTypes.string.isRequired,
        slug: React.PropTypes.string.isRequired,
        createdAt: React.PropTypes.any.isRequired,
        upvotes: React.PropTypes.number.isRequired,
        currentUser: React.PropTypes.string,
        upvoted: React.PropTypes.bool.isRequired,
    },
    childChanged: function() {
        if (!this.props.currentUser) {
            FlowRouter.go('login');
        } else if (this.props.upvoted) {
            Meteor.call('upvote', this.props._id, true);
        } else {
            Meteor.call('upvote', this.props._id);
        }
    },
    goToPost: function() {
        FlowRouter.go("singlePostbySlug", {
            slug: this.props.slug
        });
    },
    render: function() {
        var date = moment(this.props.createdAt).fromNow();
        var slug = this.props.slug;
        return (
            <li>
                <div className="feed-item">
                    <C.Upvote shape="triangle" upvotes={ this.props.upvotes } callbackParent={this.childChanged} upvoted={ this.props.upvoted } />
                    <div className="feed-item-meta" onClick={this.goToPost}>
                        <span className="feed-item-title">
                            {this.props.title}
                        </span>
                        <span className="feed-item-author">
                            {this.props.authorName}
                        </span>
                        <span className="feed-item-summary">
                            {this.props.summary}
                        </span>
                        <div className="feed-item-author-picture">
                            <img src={this.props.authorPicture} />
                        </div>
                    </div>
                </div>
            </li>
        );
    }
});
