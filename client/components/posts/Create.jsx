C.PostCreateForm = React.createClass({
    displayName: 'PostCreateForm',
    mixins: [ReactMeteorData, React.addons.LinkedStateMixin],
    getMeteorData() {
        var subCategories = Meteor.subscribe('categories');
        var categories = Categories.find({}).fetch();
        return {
            categoriesReady: subCategories.ready(),
            categories: categories,
            currentUser: Meteor.user(),
        };
    },
    getInitialState() {
        return {
            errors: {},
            text: ''
        }
    },
    getCategories() {
        var options = _.map(this.data.categories, function(doc, index) {
            var option = {};
            option.key = index;
            option.value = doc._id;
            option.label = doc.title;
            return option;
        })
        return options;
    },
    handleSubmit(event) {
        event.preventDefault();
        var title = $(event.target).find("[name=title]").val();
        var category = this.refs.category.state.selectValue;
        var summary = $(event.target).find("[name=summary]").val();
        var slug = slugify(title);
        var author = this.data.currentUser._id;
        var authorName = this.data.currentUser.profile.name;
        var authorPicture = this.data.currentUser.profile.picture;
        const published = false;
        const comments = [];
        var errors = {};

        if (!title) {
            errors.title = "A title is required"
        }

        if (!summary) {
            errors.summary = "A summary is required"
        }
        this.setState({
            errors: errors
        });

        if (!_.isEmpty(errors)) {
            return;
        }
        let date = new Date();
        var content = this.state.text;
        contentWithIds = content.replace(/<p>/g, function(m) {
            return '<p id="' + makeUniqueId() + '">';
        });
        let contentSerialized = himalaya.parse(contentWithIds);
        Posts.insert({
            'title': title,
            'slug': slug,
            'author': author,
            'authorName': authorName,
            'authorPicture': authorPicture,
            'category': category,
            'createdAt': date,
            "v": 1,
            'updatedAt': date,
            'summary': summary,
            'content': contentSerialized,
            'published': published,
            'upvotes': 0
        }, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                FlowRouter.go('myPosts');
            }
        });
    },
    handleChange(text) {
        this.setState({
            text: text
        });
    },
    render() {
        if (!this.data.categoriesReady) {
            loading = <SpinnerView />
        }
        return (
            <div className="container">
            <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <h1>Create a Post</h1>
                <form className="medium-input" onSubmit={this.handleSubmit}>
                    <C.AuthErrors errors={this.state.errors} />
                    <C.FormInput hasError={!!this.state.errors.title} name="title" type="text" label="none" />
                      <div className="form-group">
                        <C.Dropdown options={this.getCategories()} name="category" ref="category" defaultValue="sxeJcmNCGAvJ6wE7D"/>
                      </div>
                    <C.FormInput hasError={!!this.state.errors.summary} name="summary" type="textarea" label="none" />
                    {/*
                    <Editor
                      name="medium"
                      tag="pre"
                      text={this.state.text}
                      options={{placeholder: {text: 'content'}, toolbar: {buttons: ['bold', 'italic', 'underline']}}}
                      onChange={this.handleChange}
                    />*/}
                    <C.Editor
                      text={this.state.text}
                      tag="pre"
                      onChange={this.handleChange}
                    />
                    <button type="submit" className="btn btn-primary pull-right">Post</button>
                </form>
            </div>
        </div>
        )
    }
});


C.Editor = React.createClass({
  displayName: 'MediumEditor',
  getInitialState: function getInitialState() {
    return {
      text: this.props.text
    };
  },
  getDefaultProps: function getDefaultProps() {
    return {
      tag: 'div'
    };
  },

  componentDidMount: function componentDidMount() {
    var _this = this;
    var dom = React.findDOMNode(this);
    this.medium = new MediumEditor(dom, this.props.options);
    this.medium.subscribe('editableInput', function (e) {
      _this._updated = true;
      _this.change(dom.innerHTML);
    });
    $('pre').arrive('p', function () {
        $(this).removeAttr('id');
    })
  },

  componentWillUnmount: function componentWillUnmount() {
    this.medium.destroy();
    $('pre').unbindArrive("p");
  },

  componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
    if (nextProps.text !== this.state.text && !this._updated) {
      this.setState({ text: nextProps.text });
    }

    if (this._updated) this._updated = false;
  },

  render: function render() {
    return React.createElement(this.props.tag, {
      className: this.props.className,
      contentEditable: true,
      dangerouslySetInnerHTML: { __html: this.state.text }
    });
  },

  change: function change(text) {
    if (this.props.onChange) this.props.onChange(text);
  }
});
