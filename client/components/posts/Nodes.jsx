C.Paragraph = React.createClass({
    displayName: 'Paragraph',
    componentWillUpdate: function() {
        React.findDOMNode(this).classList.add("class1", "class2");
    },
    render: function() {;
        return (
          <p id={this.props.id} >{this.props.content}</p>
        );
    }
});