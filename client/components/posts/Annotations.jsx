C.AnnotationContainer = React.createClass({
    propTypes: {
        postId: React.PropTypes.string.isRequired,
        nodeId: React.PropTypes.string.isRequired,
        originalContent: React.PropTypes.string
    },
    mixins: [ReactMeteorData],
    getMeteorData() {
        var subAnnotations = Meteor.subscribe('annotations', this.props.postId, this.props.nodeId);
        var subUpvotes = Meteor.subscribe('userUpvotes', this.props.postId, 'annotation');
        var upvoteFinder = Upvotes.find({}).fetch();
        return {
            annotationsLoading: !subAnnotations.ready(),
            annotations: Annotations.find({
                nodeId: this.props.nodeId
            }, {
                sort: {
                    upvotes: -1,
                    createdAt: -1
                }
            }).fetch(),
            upvotes: upvoteFinder,
            currentUser: Meteor.userId()
        };
    },
    update: function(annotationId) {
        let annotation = Annotations.findOne({
            _id: annotationId
        });
        Meteor.call('updateContentNode', this.props.postId, this.props.nodeId, annotationId, annotation.text, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                Meteor.call('acceptAnnotation', annotationId);
            }
        });
    },
    remove: function(annotationId) {
        let annotation = Annotations.findOne({
            _id: annotationId
        });
        Meteor.call('removeAnnotation', annotationId, function(error, result) {
            if (error) {
                console.log(error);
            } else {
                return;
            }
        });
    },
    render: function() {
        let self = this;
        let annotationHeader;
        switch (this.data.annotations.length) {
            case 0:
                annotationHeader = "Suggest an Edit";
                break;
            case 1:
                annotationHeader = this.data.annotations.length + " Suggested Edit";
                break;
            default:
                annotationHeader = this.data.annotations.length + " Suggested Edits";
                break;
        }
        let accordionId = "accordion" + this.props.nodeId;
        let accordionTarget = "#" + accordionId;
        return (
            <div className="container annotation-container">
                <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1 no-padding">
                  <div className="panel panel-default">
                    <div className="accordion-heading text-center" role="tab" id="headingOne">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href={ accordionTarget } aria-expanded="true" aria-controls="collapseOne">
                            <i className="fa fa-comment-o fa-flip-horizontal"></i> { annotationHeader }
                        </a>
                    </div>
                    <div id={ accordionId } className="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                      <div className="panel-body">
                        <C.AnnotationList originalContent={ this.props.originalContent } annotations={ this.data.annotations } userUpvotes={this.data.upvotes} callbackUpdate={self.update} callbackRemove={self.remove} currentUser={this.data.currentUser}/>
                        <C.AnnotationForm value={ this.props.originalContent } postId={ this.props.postId } nodeId={ this.props.nodeId } />
                      </div>
                    </div>
                  </div>
                </div>
            </div>
        );
    }
});

C.AnnotationList = React.createClass({
    handleCallbackAccept: function(annotationId) {
        this.props.callbackUpdate(annotationId);
    },
    handleCallbackRemove: function(annotationId) {
        this.props.callbackRemove(annotationId);
    },
    render: function() {
        let self = this;
        let test = [1, 2, 3, 4];
        let annotations;
        if (this.props.annotations.length > 0) {
            annotations = _.map(this.props.annotations, function(doc, i) {
                let upvoteDoc = Upvotes.findOne({
                    type: 'annotation',
                    annotationId: doc._id,
                    user: self.props.currentUser
                });
                let upvoted;
                if (upvoteDoc) {
                    upvoted = true;
                } else {
                    upvoted = false;
                }
                return (
                    <C.AnnotationItem   callbackParentAccept={self.handleCallbackAccept.bind(self, doc._id)}
                                        callbackParentRemove={self.handleCallbackRemove.bind(self, doc._id)}
                                        originalContent={ self.props.originalContent }
                                        currentUser={self.props.currentUser}
                                        postId={doc.postId}
                                        annotationId={doc._id}
                                        annotation={ doc.text }
                                        annotatorPicture={doc.annotatorPicture}
                                        annotatorName={doc.annotatorName}
                                        createdAt={doc.createdAt}
                                        upvoted= { upvoted }
                                        upvotes={doc.upvotes}
                                        key={ i } />
                )
            })
        } else {
            annotations = <div></div>
        }
        return (
            <div className="annotation-list">
                { annotations }
            </div>
        )
    }
});

C.AnnotationItem = React.createClass({
    propTypes: {
        postId: React.PropTypes.string.isRequired,
        annotationId: React.PropTypes.string.isRequired,
        annotatorName: React.PropTypes.string.isRequired,
        annotatorPicture: React.PropTypes.string.isRequired,
        createdAt: React.PropTypes.string.isRequired,
        currentUser: React.PropTypes.string,
        upvoted: React.PropTypes.bool.isRequired,
        upvotes: React.PropTypes.number.isRequired
    },
    upvote: function() {
        if (!this.props.currentUser) {
            FlowRouter.go('login');
        } else if (this.props.upvoted) {
            Meteor.call('upvoteAnnotation', this.props.postId, this.props.annotationId, true);
        } else {
            Meteor.call('upvoteAnnotation', this.props.postId, this.props.annotationId);
        }
    },
    handleAccept: function() {
        this.props.callbackParentAccept();
    },
    handleRemove: function() {
        this.props.callbackParentRemove();
    },
    render: function() {
        self = this;
        var diff = JsDiff.diffWords(self.props.originalContent, self.props.annotation);
        var diffParts = _.map(diff, function(doc, index) {
            if (doc.added) {
                return <C.AnnotationCharacter style="added" content={doc.value} key={index} />
            } else if (doc.removed) {
                return <C.AnnotationCharacter style="removed" content={doc.value} key={index} />
            } else {
                return <C.AnnotationCharacter content={doc.value} key={index} />
            }
        })
        var date = moment(this.props.createdAt).fromNow();
        return (
            <div className="annotation-item">
                    <div className="annotation-left">
                        <div className="annotation-content">
                        <C.UserAvatar imageSource={this.props.annotatorPicture} />
                        <div className="annotation-meta">
                            <p>{this.props.annotatorName}</p> <span className="date sub-text">{ date }</span>
                        </div>
                        <div className="annotation-text">
                            { diffParts }
                        </div>
                        </div>
                    <C.Upvote shape= { "triangle" } upvotes= {this.props.upvotes } callbackParent={this.upvote} upvoted={ this.props.upvoted } />
                    </div>
                    <div className="annotation-utility">
                        <button onClick={this.handleAccept} className="btn btn-primary pull-right">Accept</button>
                        <button onClick={this.handleRemove} className="btn btn-primary pull-right">Delete</button>
                    </div>
                </div>

        )
    }
})

C.AnnotationCharacter = React.createClass({
    render: function() {
        var className;
        if (this.props.style) {
            className = this.props.style + " " + "part";
        } else {
            className = "part";
        }
        return (
            <span className={className}>{this.props.content}</span>
        );
    }
})

C.AnnotationForm = React.createClass({
    displayName: "AnnotationForm",
    propTypes: {
        value: React.PropTypes.string,
    },
    getInitialState() {
        return {
            errors: {},
            text: ''
        }
    },
    handleSubmit(event) {
        event.preventDefault();
        var postId = this.props.postId;
        var nodeId = this.props.nodeId;
        var title = $(event.target).find("[name=title]").val();
        var errors = {};
        if (!title) {
            errors.title = "A title is required"
        }
        this.setState({
            errors: errors
        });
        if (!_.isEmpty(errors)) {
            return;
        }
        Meteor.call('insertAnnotation', postId, nodeId, title);
    },
    render: function() {
        return (
            <div className="annotation-form row">
                <form onSubmit={this.handleSubmit}>
                    <C.AuthErrors errors={this.state.errors} />
                    <C.FormInput hasError={!!this.state.errors.title} name="title" type="textarea" label="none" value={this.props.value} />
                    <button type="submit" className="btn btn-sm btn-primary pull-right">Submit</button>
                </form>
            </div>
        )
    }
});
