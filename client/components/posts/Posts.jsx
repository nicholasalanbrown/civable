C.PostContainer = React.createClass({
    displayName: 'PostContainer',
    mixins: [ReactMeteorData],
    getMeteorData() {
        if (this.props.postSlug) {
            var handle = Meteor.subscribe("singlePostbySlug", this.props.postSlug);
        } else {
            var handle = Meteor.subscribe("singlePostbyId", this.props.postId);
        }
        return {
            postLoading: !handle.ready(),
            post: Posts.findOne(),
            currentUser: Meteor.user()
        };
    },
    render: function() {
        if (this.data.postLoading) {
            return <SpinnerView />
        } else {
            let postUtility;
            let commentForm;
            if (this.data.currentUser) {
                if (this.data.currentUser._id === this.data.post.author) {
                    postUtility = <C.PostUtility postId={this.data.post._id}/>
                }
                commentForm = <C.CommentForm postId={ this.data.post._id } currentUserId={ this.data.currentUser._id } currentUserName={ this.data.currentUser.profile.name } currentUserPicture={ this.data.currentUser.profile.picture } />;
            } else {
                commentForm = <C.CommentLoggedOut />;
            }
            return (
                <div>
                    <C.PostHeader postId={ this.data.post._id } title={this.data.post.title} summary={this.data.post.summary} author={this.data.post.author} authorName={ this.data.post.authorName } createdAt={this.data.post.createdAt} />
                    <div className="container">
                        <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            {postUtility}
                            <C.PostContent content={this.data.post.content} postId={this.data.post._id} />
                            <C.CommentList postId={ this.data.post._id } />
                            { commentForm }
                        </div>
                    </div>
                </div>
            );
        }
    }
});

C.PostHeader = React.createClass({
    displayName: 'PostHeader',
    propTypes: {
        postId: React.PropTypes.string,
        title: React.PropTypes.string.isRequired,
        summary: React.PropTypes.string.isRequired,
        author: React.PropTypes.string.isRequired,
        authorName: React.PropTypes.string.isRequired,
        createdAt: React.PropTypes.any.isRequired,
    },
    render: function() {
        let {
            title, summary, author, authorName, createdAt
        } = this.props;
        var date = moment(this.props.createdAt).fromNow();
        return (
            <div className="post-header v-center">
                <div className="post-heading container">
                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <h1>{ title }</h1>
                    </div>
                </div>
                <C.AnnotationContainer postId={ this.props.postId } nodeId="title" originalContent={ this.props.title } />
                <div className="container">
                    <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                        <p className="post-summary">{ summary }</p>
                        <C.AnnotationContainer postId={ this.props.postId } nodeId='summary' originalContent={ this.props.summary } />
                        <p>Posted by { authorName } { date }</p>
                    </div>
                </div>
            </div>
        );
    }
});

C.PostBody = React.createClass({
    displayName: 'PostBody',
    propTypes: {
        content: React.PropTypes.string.isRequired,
        postId: React.PropTypes.string.isRequired,
    },
    componentDidMount: function() {
        var self = this;
        $(".post-body p").each(function() {
            let newDiv;
            if ($(this).attr('id')) {
                var nodeId = $(this).attr('id');
                newDiv = '<div id="inlinecomments-' + $(this).attr('id') + '"></div>';
                var divId = "inlinecomments-" + $(this).attr('id');
                $(newDiv).insertAfter(this);
                React.render(<C.AnnotationContainer postId={self.props.postId} nodeId={ nodeId } />, document.getElementById(divId));
            } else {
                blankDiv = '<div></div>';
                $(blankDiv).insertAfter(this);
            }
        });
    },
    render: function() {
        let {
            content
        } = this.props;
        var rawContent = content.toString();

        function createMarkup() {
            return {
                __html: rawContent
            };
        };
        return (
            <div className="post-body">
              <div dangerouslySetInnerHTML={ createMarkup() } />
            </div>
        );
    }
});

C.PostContent = React.createClass({
    displayName: 'PostContent',
    propTypes: {
        postId: React.PropTypes.string.isRequired,
        content: React.PropTypes.array.isRequired
    },
    render: function() {;
        let self = this;
        return (
            <div className="post-content">
                    {
                      _.map(this.props.content, function (doc, index) {
                            let component;
                            switch (doc.tagName) {
                                case "p":
                                    component = 
                                      <div key={ index } >
                                        <C.Paragraph content={doc.children[0].content} id={doc.attributes.id} />
                                        <C.AnnotationContainer postId={ self.props.postId } nodeId={ doc.attributes.id } originalContent={ doc.children[0].content } />
                                      </div>
                                    break;
                            }
                            return component;
                        })
                    }
            </div>
        );
    }
});

C.PostUtility = React.createClass({
    displayName: 'PostUtility',
    propTypes: {
        postId: React.PropTypes.string.isRequired,
    },
    edit: function() {
        FlowRouter.go('editPost', {
            _id: this.props.postId
        });
    },
    render: function() {
        return (
            <div>
                <button onClick={this.edit}>Edit!</button>
            </div>
        );
    }
});
