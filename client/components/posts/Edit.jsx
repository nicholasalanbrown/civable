C.PostEditForm = React.createClass({
    displayName: 'PostEditForm',
    mixins: [ReactMeteorData, React.addons.LinkedStateMixin],
    getMeteorData() {
        var handle = Meteor.subscribe("singlePostbyId", this.props.postId);
        var subCategories = Meteor.subscribe('categories');
        var categories = Categories.find({}).fetch();
        var postFinder = Posts.findOne();
        return {
            postLoading: !handle.ready(),
            post: postFinder,
            categoriesReady: subCategories.ready(),
            categories: categories,
            currentUser: Meteor.user()
        };
    },
    getInitialState() {
        return {
            errors: {}        }
    },
    getCategories() {
        var options = _.map(this.data.categories, function(doc, index) {
            var option = {};
            option.key = index;
            option.value = doc._id;
            option.label = doc.title;
            return option;
        })
        return options;
    },
    handleChange(text) {
        this.setState({
            text: text
        });
    },
    handleSubmit(event) {
        event.preventDefault();
        let self = this;
        var previousWithId = Posts.findOne();
        var previous = Posts.findOne({}, {
            fields: {
                _id: 0
            }
        });
        var currVersion = previous.v;
        var title = $(event.target).find("[name=title]").val();
        var category = $(event.target).find("[name=category]").val();
        var summary = $(event.target).find("[name=summary]").val();
        var slug = slugify(title);
        var author = this.data.currentUser._id;
        var published = false;
        var errors = {};
        if (!title) {
            errors.title = "A title is required"
        }

        if (!summary) {
            errors.summary = "A summary is required"
        }
        this.setState({
            errors: errors
        });

        if (!_.isEmpty(errors)) {
            return;
        }
        let content = this.state.text;
        let contentWithIds = content.replace(/<p>/g, function(m) {
            return '<p id="' + makeUniqueId() + '">';
        });
        let contentSerialized = himalaya.parse(contentWithIds);
        var current = {
            title: title,
            slug: slug,
            category: category,
            updatedAt: new Date(),
            summary: summary,
            content: contentSerialized,
            published: published,
            v: previous.v + 1
        };
        Meteor.call('updatePost', previous, previousWithId._id, current, function(error, result) {
            if (error) {
                console.log(error.reason);
            } else {
                FlowRouter.go('singlePostbyId', {
                    _id: self.data.post._id
                });
            }
        });
    },
    handleDelete() {
        Posts.remove({
            _id: this.data.post._id
        }, function(error, result) {
            if (error) {
                console.log(error.reason)
            } else {
                FlowRouter.go('posts');
            }
        })
    },
    render() {
        if (this.data.postLoading) {
            return <C.LoadingSpinner />
        } else {
            const {
                title, category, summary, content
            } = this.data.post;
            let contentString = toHTML(this.data.post.content);
            return (
                <div className="container">
                    <div className="row">
                        <div className="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                            <h1>Edit Post</h1>
                            <form className="medium-input" onSubmit={this.handleSubmit}>
                                <C.AuthErrors errors={this.state.errors} />
                                <C.FormInput hasError={!!this.state.errors.title} name="title" type="text" value= { title } label="none"/>
                                  <div className="form-group">
                                    <C.Dropdown options={this.getCategories()} name="category" ref="category" value={ category } />
                                  </div>
                                <C.FormInput hasError={!!this.state.errors.summary} name="summary" type="textarea" value={ summary } label="none" />
                                {/*<C.FormInput hasError={!!this.state.errors.content} name="content" type="textarea" value={ content } label="none" />*/}
                                {/*}
                                <Editor
                                  name="content"
                                  tag="pre"
                                  text={this.data.post.content}
                                  options={{toolbar: {buttons: ['bold', 'italic', 'underline']}}}
                                  onChange={this.handleChange}
                                  />
                                */}
                                <C.Editor
                                  tag="pre"
                                  text={ contentString }
                                  onChange={this.handleChange}
                                />
                                 <div className="form-group">
                                    <button className="btn btn-danger pull-right" onClick={this.handleDelete}>Delete</button>
                                    <button type="submit" className="btn btn-primary pull-right">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            )
        }
    }
});
