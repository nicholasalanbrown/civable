C.FormInput = React.createClass({
    propTypes: {
        hasError: React.PropTypes.bool,
        label: React.PropTypes.string,
        type: React.PropTypes.string,
        name: React.PropTypes.string,
        ref: React.PropTypes.string,
        value: React.PropTypes.string,
        onKeyUp: React.PropTypes.func,
        onBlur: React.PropTypes.func
    },
    shouldComponentUpdate() {
        return true;
    },
    render() {
        const {type, label, name, ref, value, onKeyUp, onBlur } = this.props;
        let inputType;

        var className = "form-group";
        if (this.props.hasError) {
            className += " has-error";
        }

        switch (type) {
            case "textarea":
                inputType = (
                  <textarea type={ type } className="form-control" name={ name.toLowerCase() } ref={ name } placeholder={ name } defaultValue={ value } onKeyUp={ onKeyUp } onBlur={ onBlur }></textarea>
                );
                break;
            default:
                inputType = (
                  <input type={ type } className="form-control" name={ name.toLowerCase() } ref={ name } placeholder={ name } defaultValue={ value } onKeyUp={ onKeyUp } onBlur={ onBlur }/>
                );
                break;
        }


        return (
          <div className={ className }>
              { label === "none" ? "" : <label htmlFor={ name.toLowerCase() } className="control-label">{ name }</label> }
              { inputType }
          </div>
        )

    }
});


C.AuthErrors = React.createClass({
    propTypes: {
        errors: React.PropTypes.object
    },
    render() {
        if (this.props.errors) {
            return (
                <ul className="list-group">
                    {
                        _.values(this.props.errors).map((errorMessage) => {
                            return <li key={errorMessage} className="list-group-item alert alert-danger">{errorMessage}</li>;
                        })
                    }
                </ul>
            )
        }
    }
});



C.Dropdown = React.createClass({
    displayName: 'Dropdown',
    getInitialState:function(){
    if (this.props.value) {
        return {selectValue: this.props.value};
    }
    else {
      return {selectValue: this.props.defaultValue};
    }
  },
    handleChange:function(e){
    this.setState({selectValue:e.target.value});
  },
  render: function() {
    let self = this;
    var categories = _.map(this.props.options, function (doc, index) {
      return (
        <option value={doc.value} ref={self.props.name} key={index} >
          {doc.label}
        </option>
      );
    })
    return (
      <select
        value={this.state.selectValue} 
        onChange={this.handleChange}
        className="form-control" 
      >
        {categories}
      </select>
    );
    }
});