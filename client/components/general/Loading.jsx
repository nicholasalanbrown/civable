C.LoadingSpinner = React.createClass({
    render: function() {
        return (
	    	<div className="loading-container h-center">
	            <SpinnerView />
	        </div>
        );
    }
});