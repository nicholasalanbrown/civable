

C.Upvote = React.createClass({
    propTypes: {
        shape: React.PropTypes.oneOf(['triangle', 'heart']).isRequired,
        upvoted: React.PropTypes.bool.isRequired,
    },
    handleClick: function () {
    	this.props.callbackParent();
    },
	render() {
		let shape = this.props.shape;
		let upvoted;
		if (this.props.upvoted) {
			upvoted = "already-voted"
		}
		else {
			upvoted = ""
		}
		let classNames = "vote-button h-center " + upvoted;
		return (
			<div className="vote-container text-center h-center" onClick={this.handleClick} >
				<div className={ classNames }>
					<div className={shape}>
					</div>
					<span className="vote-count">{ this.props.upvotes }</span>
				</div>
			</div>
		);
	}
});
