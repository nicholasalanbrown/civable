C.Menu = React.createClass({
    getDefaultProps: function() {
        return {
            visible: false
        };
    },
    handleClick: function () {
        this.props.callbackParent();
    },
    render: function() {
        return (
            <div>
                <div className={"menu-inactive"}>
                    <div onClick={this.handleClick} className={(this.props.visible ? "visible " : "") + this.props.alignment}></div>
                </div>
                <div className="menu">
                    <div className={(this.props.visible ? "visible " : "") + this.props.alignment}>{this.props.children}</div>
                </div>
            </div>
            )
    }
});

C.MenuItem = React.createClass({
    navigate: function(hash) {
        window.location.hash = hash;
    },

    render: function() {
        return <div className="menu-item" onClick={this.navigate.bind(this, this.props.hash)}>{this.props.children}</div>;
    }
});
