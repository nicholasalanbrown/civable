C.MainHeader = React.createClass({
    mixins: [ReactMeteorData],
    getMeteorData() {
        return {
            currentUser: Meteor.user()
        }
    },
    handleLogout() {
        Meteor.logout(function(err) {
            FlowRouter.go("/login");
        });
    },
    handleToggle() {
        this.props.callbackParent();
    },
    render() {
        let linkList;
        let { currentUser } = this.data;

        if (currentUser) {
            linkList = (
                <div className="collapse navbar-collapse">
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href={FlowRouter.path('home')}>Home</a></li>
                        <li><a href={FlowRouter.path('myPosts')}>My Posts</a></li>
                        <li><a href={FlowRouter.path('create')}>Create</a></li>
                        <li><a href="#" onClick={this.handleLogout}>Logout</a></li>
                    </ul>
                </div>
            )
        } else {
            linkList = (
                <div className="collapse navbar-collapse">
                    <ul className="nav navbar-nav navbar-right">
                        <li><a href={FlowRouter.path('home')}>Home</a></li>
                        <li><a href={FlowRouter.path('login')}>Login</a></li>
                        <li><a href={FlowRouter.path('signup')}>Sign Up</a></li>
                    </ul>
                </div>
            )
        }
        return (
            <Headroom>
            <nav className="navbar navbar-default">
                <div className="navbar-header">
                  <button type="button" onClick={this.handleToggle} className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span className="sr-only">Toggle navigation</span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                    <span className="icon-bar"></span>
                  </button>
                    <a className="navbar-brand" href={FlowRouter.path('home')}>
                        <img src="/images/logo.png" alt="Civable logo" id="navbar-image" />
                    </a>
                </div>
                <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    {linkList}
                </div>
            </nav>
            </Headroom>
        )
    }
});

C.SubNav = React.createClass ({
    render() {
        return (
            <div className="subnav">
            </div>
        );
    }
})
