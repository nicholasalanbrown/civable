C.MainLayout = React.createClass({
    getInitialState: function () {
        return {showMenu: false}
    },
    showLeft: function() {
        this.refs.left.show();
    },

    showRight: function() {
        this.refs.right.show();
    },
    toggleMenu: function () {
        this.setState({
            showMenu: !this.state.showMenu
        });
    },
    render() {
        if (this.state.showMenu) {
            document.body.style.overflowY = "hidden";
        }
        else {
            document.body.style.overflowY = "scroll";
        }
        return (
            <div className="full-height">
                <C.Menu ref="right" alignment="right" visible={this.state.showMenu} callbackParent={this.toggleMenu}>
                    <ul>
                        <li><a className="menu-item" onClick={this.toggleMenu} href={FlowRouter.path('home')}>Home</a></li>
                        <li><a className="menu-item" onClick={this.toggleMenu} href={FlowRouter.path('myPosts')}>My Posts</a></li>
                        <li><a className="menu-item" onClick={this.toggleMenu} href={FlowRouter.path('create')}>Create</a></li>
                        <li><a className="menu-item" onClick={this.toggleMenu} href="#" onClick={this.handleLogout}>Logout</a></li>
                    </ul>
                </C.Menu>

                <C.MainHeader callbackParent={this.toggleMenu}/>

                {this.props.content}

                {this.props.footer}

            </div>
        )
    }
});