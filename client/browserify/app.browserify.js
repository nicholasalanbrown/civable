//All NPM modules listed in packages.json get required here and exposed as global functions, including React itself (which is actually added via Meteor's native package system, not NPM)

React = require('react');
Headroom = require("react-headroom");
Modal = require('react-bootstrap').Modal;
Reactable = require('reactable');
Table = Reactable.Table;
Tr = Reactable.Tr;
JsDiff = require('diff');
himalaya = require('himalaya');
toHTML = require('himalaya/translate').toHTML;
Menu = require('react-burger-menu').slide;