//Coniguring a package that ensures the page is rendered scrolled to the top instead of where the user left off. This seems like a bug in FlowRouter, I'm not sure why it requires an outside package to fix. This needs to be declared only only on the client since the package isn't accessible on the server.

FlowRouterAutoscroll.animationDuration = 0;