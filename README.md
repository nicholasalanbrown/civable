First, install Meteor - there's simple instructions in the docs.
To get things running with the current version of Meteor, you'll want ot do run:
```
meteor
```
then likely:
```
rm -rf packages/npm-container
meteor remove meteorhacks:npm
meteor add meteorhacks:npm
meteor //NPM support should now be added
meteor
```
There appears to be some issue with the new version of NPM. I'm not sure what the fix is right now.

