var categories_v1 = [
	{
        "title": "Agriculture and Food"
    }, {
        "title": "Animals"
    }, {
        "title": "Armed Forces and National Security"
    }, {
        "title": "Arts"
    }, {
        "title": "Civil Rights and Liberties"
    }, {
        "title": "Commerce"
    }, {
        "title": "Congress"
    }, {
        "title": "Crime and Law Enforcement"
    }, {
        "title": "Economics and Public Finance"
    }, {
        "title": "Education"
    }, {
        "title": "Emergency Management"
    }, {
        "title": "Energy"
    }, {
        "title": "Environmental Protection"
    }, {
        "title": "Families"
    }, {
        "title": "Finance and Financial Sector"
    }, {
        "title": "Foreign Trade and International Finance"
    }, {
        "title": "Government Operations and Politics"
    }, {
        "title": "Health"
    }, {
        "title": "Housing and Community Development"
    }, {
        "title": "Immigration"
    }, {
        "title": "International Affairs"
    }, {
        "title": "Labor and Employment"
    }, {
        "title": "Law"
    }, {
        "title": "Native Americans"
    }, {
        "title": "Public Lands and Natural Resources"
    }, {
        "title": "Science"
    }, {
        "title": "Social Sciences and History"
    }, {
        "title": "Social Welfare"
    }, {
        "title": "Sports and Recreation"
    }, {
        "title": "Taxation"
    }, {
        "title": "Transportation and Public Works"
    }, {
        "title": "Water Resources Development"
    }
]

Migrations.add({
    version: 1,
    up: function() {
        Categories.remove({});
        _.each(categories_v1, function(doc) {
            Categories.insert(doc);
        });
    }
});
