//These functions control what rights types of users have to edit Mongo collections. This is one approach to authorization, usually assuming you're just using "Posts.insert()" on the front end. I've since moved to using Meteor methods, which allow me to aggregate all of my allowed database operations in one place.

Meteor.users.deny({
  update: function() {return true;}
});

Posts.allow({
  insert: function (userId, doc) {
    return (userId && doc.author === userId);
  },
  update: function (userId, doc, fields, modifier) {
    return doc.author === userId;
  },
  remove: function (userId, doc) {
    return doc.author === userId;
  },
  fetch: ['author']
});

Comments.allow({
  insert: function (userId, doc) {
    return userId;
  },
  update: function (userId, doc, fields, modifier) {
    // can only change your own documents
    return doc.commenter === userId;
  },
  remove: function (userId, doc) {
    // can only remove your own documents
    return doc.commenter === userId;
  }
});