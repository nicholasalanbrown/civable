//Methods that manipulate post data

Meteor.methods({
    updatePost: function(previous, previousId, current) {
        var result = Posts_History.update({
            "postId": previousId,
            "v": previous.v
        }, {
            "$set": previous
        }, {
            "upsert": true
        });
        var result = Posts.update({
            _id: previousId,
            v: previous.v
        }, {
            $set: current
        })
        if (result != 1) {
            console.log('Somebody got there first!');
        } else {
            return result;
        }
    },

//This method updates nested HTML nodes within the "content" key

    updateContentNode: function(postId, nodeId, annotationId, text) {
        var previous = Posts.findOne({
            _id: postId
        }, {
            fields: {
                _id: 0
            }
        });
        var result = Posts_History.update({
            "postId": postId,
            "v": previous.v
        }, {
            "$set": previous
        }, {
            "upsert": true
        });
        switch (nodeId) {
            case 'title':
                var result = Posts.update({
                    _id: postId
                }, {
                    $set: {
                        title: text
                    }
                });
                if (result != 1) {
                    console.log('Somebody got there first!');
                } else {
                    return result;
                }
                break;
            case 'summary':
                var result = Posts.update({
                    _id: postId
                }, {
                    $set: {
                        summary: text
                    }
                });
                if (result != 1) {
                    console.log('Somebody got there first!');
                } else {
                    return result;
                }
                break;
            default:
                var previousNode = _.find(previous.content, function(node) {
                    return node.attributes.id === nodeId;
                });
                previousNode.children[0].content = text;
                var result = Posts.update({
                    _id: postId
                }, {
                    $set: {
                        content: previous.content
                    }
                });
                if (result != 1) {
                    console.log('Somebody got there first!');
                } else {
                    return result;
                }
                break;
        }

    }
});
