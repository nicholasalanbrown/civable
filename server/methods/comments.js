//Methods that manipulate comment data in Mongo.
//TODO: add the ability to remove or update comments

Meteor.methods({
    insertComment: function(postId, commenter, commenterName, commenterPicture, text) {
        Comments.insert({
        	'postId': postId,
            'commenterId': commenter,
            'commenterName': commenterName,
            'commenterPicture': commenterPicture,
            'text': text,
            'createdAt': new Date(),
            'upvotes': 0
        });
        return;
    }
});
