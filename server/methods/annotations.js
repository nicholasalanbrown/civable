//Functions to manipulate annotation data in the database. Right now, I'm thinking of any suggested edit to the content as an "annotation," thought that might not actually be the right word.

Meteor.methods({
    insertAnnotation: function(postId, nodeId, text) {
        Annotations.insert({
        	'postId': postId,
            "nodeId": nodeId,
            'annotatorId': Meteor.userId(),
            'annotatorName': Meteor.user().profile.name,
            'annotatorPicture': Meteor.user().profile.picture,
            'text': text,
            'createdAt': new Date(),
            'upvotes': 0
        });
        return;
    },
    acceptAnnotation: function(annotationId) {
        Annotations.remove({
            _id: annotationId
        });
        return;
    },
    removeAnnotation: function(annotationId) {
        Annotations.remove({
            _id: annotationId
        });
        return;
    }
});
