//Methods for upvoting. Not sure if this should be one function or several depending on whether it's a post, comment or annotation?

Meteor.methods({
    upvote: function(postId, removeUpVote) {
        var user = Meteor.userId();
        if (removeUpVote) {
            Posts.update({
                _id: postId
            }, {
                $inc: {
                    upvotes: -1
                }
            });
            Upvotes.remove({
                postId: postId,
                type: 'post',
                user: user
            })
        } else {
            Posts.update({
                _id: postId
            }, {
                $inc: {
                    upvotes: 1
                }
            });
            Upvotes.insert({
                postId: postId,
                type: 'post',
                user: user
            })
        }
    },
    upvoteComment: function(postId, commentId , removeUpVote) {
        var user = Meteor.userId();
        if (removeUpVote) {
            Comments.update({
                _id: commentId,
                postId: postId
            }, {
                $inc: {
                    upvotes: -1
                }
            });
            Upvotes.remove({
                postId: postId,
                type: "comment",
                commentId: commentId,
                user: user
            })
        } else {
            Comments.update({
                _id: commentId,
                postId: postId
            }, {
                $inc: {
                    upvotes: 1
                }
            });
            Upvotes.insert({
                postId: postId,
                type: "comment",
                commentId: commentId,
                user: user
            })
        }
    },
    upvoteAnnotation: function(postId, annotationId , removeUpVote) {
        var user = Meteor.userId();
        if (removeUpVote) {
            Annotations.update({
                _id: annotationId,
                postId: postId
            }, {
                $inc: {
                    upvotes: -1
                }
            });
            Upvotes.remove({
                postId: postId,
                type: "annotation",
                annotationId: annotationId,
                user: user
            })
        } else {
            Annotations.update({
                _id: annotationId,
                postId: postId
            }, {
                $inc: {
                    upvotes: 1
                }
            });
            Upvotes.insert({
                postId: postId,
                type: "annotation",
                annotationId: annotationId,
                user: user
            })
        }
    }
});
