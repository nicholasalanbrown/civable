//These functions make data accessible to the client, to be accessed by "Meteor.subscribe()" functions.

Meteor.publish('userData', function() {
    return Meteor.users.find({
        _id: this.userId
    }, {
        fields: {
            services: 1
        }
    });
})

Meteor.publish('firstPosts', function(limit, isPublished) {
    return [
        Posts.find({
            published: isPublished
        }, {
            limit: limit,
            sort: {
                createdAt: -1
            }
        })
    ];
});

Meteor.publish('posts', function(limits, isPublished) {
    return [
        Posts.find({
            published: isPublished
        }, {
            limit: limits.posts,
            sort: {
                createdAt: -1
            }
        })
    ];
});

Meteor.publish('userPosts', function(user) {
    return [
        Posts.find({
            author: user
        },
        {
            sort: {
                createdAt: -1
            }
        }
        )
    ];
});


Meteor.publish('singlePostbyId', function(postId) {
    return [
        Posts.find({
            _id: postId
        })
    ];
});

Meteor.publish('singlePostbySlug', function(postSlug) {
    return [
        Posts.find({
            slug: postSlug
        })
    ];
});

Meteor.publish('categories', function() {
    return Categories.find({});
});

Meteor.publish('comments', function(postId) {
    return Comments.find({
        postId: postId
    });
});

Meteor.publish('userUpvotes', function(postId, type) {
    if (postId && type) {
        return Upvotes.find({postId: postId, type: type, user: this.userId});
    }
    else {
        return Upvotes.find({type: type, user: this.UserId});
    }
});

Meteor.publish('feedUpvotes', function(type) {
    return Upvotes.find({type: type, user: this.userId});
});


Meteor.publish('postUpvotes', function (postId) {
  Counts.publish(this, 'postUpvoteNumber', Posts.find({postId: postId}));
});

Meteor.publish('annotations', function(postId, nodeId) {
    return Annotations.find({postId: postId, nodeId: nodeId});
});
