//The below code imports Facebook profile information into the Meteor.users collection documents when accounts are created.

Accounts.onCreateUser(function(options, user) {
    if (options.profile) {
        if (checkNested(user, "services", "facebook")) {
            options.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
        }
		else if (checkNested(options, "profile", "picture")) {
            user.profile = options.profile;
        }
        else {
            options.profile.picture = "/images/user.png";
        }
        user.profile = options.profile;
    }
    return user;
});
