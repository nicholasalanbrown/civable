//Loading in fake data to be used for UI design. "Meteor.startup()" functions on the server run anytime the server restarts.

Meteor.startup(function() {
    if (Meteor.users.find().count() === 0) {
        Accounts.createUser({
            email: "nick@nick.com",
            password: "password",
            profile: {
                name: "Nick"
            }
        });
    }
    if (Posts.find().count() < 50 || Meteor.users.find().count < 1) {
        _.each(_.range(20), function() {
            var randomEmail = faker.internet.email();
            var randomPassword = faker.internet.password();
            var randomTitle = faker.lorem.sentence();
            var slug = slugify(randomTitle);
            var randomName = faker.name.findName();
            var randomPicture = faker.image.avatar();
            var randomDate = faker.date.past();
            var randomSummary = faker.lorem.sentences();
            var randomContent = [{
                "tagName": "p",
                "attributes": {
                    "id": "6kqj1"
                },
                "children": [{
                    "content": "Lorem ipsum dolor sit amet, ex est essent diceret dolores. Ne noster laoreet vis, his accusam cotidieque ex. Vis an noster essent facete, ea pri nominavi laboramus dissentiunt. Id quem labitur mel.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "D6CtM"
                },
                "children": [{
                    "content": "Scripta virtute ut , erat clita primis ad eos. Ei qui ipsum dicat. Mei tantas quaeque eu. Dico unum adolescens qui ea, vel ea eirmod incorrupte argumentum.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "3pMIh"
                },
                "children": [{
                    "content": "Et enim eirmod detraxit sit, solet mediocrem ei vix. Appareat expetendis eu mea, nam in constituto sententiae repudiandae, id nam tale labitur salutatus. Eu usu vidisse ornatus delicatissimi, alia malis et sit, vel id aliquam delectus gubergren. Quem possim mediocritatem no quo, soluta theophrastus usu an. Mollis maluisset pri te.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "3IslB"
                },
                "children": [{
                    "content": "Debitis urbanitas elaboraret ne qui. Mei affert inermis eu, in mei cibo novum contentiones. Te aperiri labitur eos, malis vivendum intellegebat sea in, id equidem nominavi accusamus mea. Simul audire eos at, docendi deleniti no nam. Ex iisque electram nec, minim laoreet vix ex. Veniam epicurei ea eum, pri cu dicat suavitate deseruisse.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "MNyes"
                },
                "children": [{
                    "content": "Pri eu lobortis dignissim. Ius utinam vituperatoribus te. At duo habeo mediocrem qualisque, accumsan suscipit sapientem eam eu, an veritus voluptua copiosae quo. Mel dicam eruditi no, cum an dolor efficiantur. Est case prodesset no, case fugit iracundia ei vel.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "IcU89"
                },
                "children": [{
                    "content": "Mea ferri nostro recteque et. Prompta expetenda ad est, in fierent detracto tractatos duo. Inani abhorreant theophrastus qui eu, per id aliquam assueverit. Integre invenire assentior qui in. An sit dolore perfecto phaedrum, mea an euismod epicuri. Ex feugiat delicata vix, suas impedit disputationi et has.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "nawxZ"
                },
                "children": [{
                    "content": "Evertitur torquatos disputando at vim, at sea dicit iudicabit. Mea ne quot illud menandri, vel verterem referrentur ne. His cu aliquip nominavi hendrerit, elit integre mel eu, partem sensibus mea no. Mei aliquip discere liberavisse eu, nam id eius meis dolore, vel elitr saperet mentitum an. Ea convenire interpretaris vix, qui erat posse placerat no.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "Jp9hp"
                },
                "children": [{
                    "content": "Vel te bonorum indoctum, ius fuisset euripidis reprimique ea. Vel idque dolore facete in, qui iisque argumentum an, graeco blandit mel ne. Diam facer gubergren ius eu, ius dico viderer suavitate ad. Vim cu errem timeam vocibus, eos soleat ceteros fierent id, delectus facilisi has ne. An cum percipit similique.",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "8GrMS"
                },
                "children": [{
                    "content": "Ei pri audire scripserit, magna mazim accusam cu mea. Ius probo offendit dissentias te, mazim dolorum ex ius. An vel lorem postea reprimique, ex est cibo philosophia. Ea cum commune efficiendi. Ad vis cibo tamquam forensibus, sit eu augue possim disputando. Per ut nobis pl",
                    "type": "Text"
                }],
                "type": "Element"
            }, {
                "tagName": "p",
                "attributes": {
                    "id": "1v3Ki"
                },
                "children": [{
                    "tagName": "span",
                    "attributes": {
                        "style": {
                            "letterSpacing": "0.01rem",
                            "lineHeight": 1.5000000000000000
                        }
                    },
                    "children": [{
                        "content": "Ei sit inermis verterem gloriatur, omnis graece est ex. Eu sed idque placerat reformidans, id molestiae consequat efficiantur nec. Eu utinam torquatos adversarium vim. An iisque neglegentur per.",
                        "type": "Text"
                    }],
                    "type": "Element"
                }],
                "type": "Element"
            }]
            var profile = {
                name: randomName,
                picture: randomPicture
            };
            Accounts.createUser({
                email: randomEmail,
                password: randomPassword,
                profile: profile
            });
            Posts.insert({
                title: randomTitle,
                slug: slug,
                author: randomEmail,
                authorName: randomName,
                authorPicture: randomPicture,
                category: "Category 1",
                createdAt: randomDate,
                updatedAt: randomDate,
                tags: "tag",
                summary: randomSummary,
                content: randomContent,
                published: true,
                upvotes: 0,
                v: 1
            }, function(error, result) {
                if (error) {
                    console.log(error);
                } else {
                    return;
                }
            })
        });
    }

//These functions create a database entry that configures Facebook login

    ServiceConfiguration.configurations.remove({
        service: "facebook"
    });
    ServiceConfiguration.configurations.insert({
        service: "facebook",
        appId: "600380883426061",
        loginStyle: "popup",
        secret: "ac7932431cfbc8b68f58347bd866f56b"
    });
})
