//Collection creation and schema declaration for psots. I commented out the schema because I still haven't figured out to declare the content key schema so it allows all of the possible nested documents that might be entered.

Posts = new Mongo.Collection("posts");

//Posts_History stores previous versions of posts on revision.
//TODO: Create a schema for Posts_History
Posts_History = new Mongo.Collection("posts_history");

Scema = {};

/*

Posts.attachSchema(new SimpleSchema({
    author: {
        type: String,
        optional: false
    },
    authorName: {
        type: String,
        optional: false
    },
    authorPicture: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: false
    },
    title: {
        type: String,
        optional: false
    },
    slug: {
        type: String,
        optional: false
    },
    summary: {
        type: String,
        optional: false
    },
    category: {
        type: String,
        optional: false
    },
    content: {
        type: String,
        optional: false
    },
    contentSerialized: {
        type: [Object],
        optional: true
    },
    createdAt: {
        type: Date
    },
    v: {
        type: Number,
        optional: false
    },
    updatedAt: {
        type: Date
    },
    published: {
        type: Boolean,
        optional: false
    },
    upvotes: {
        type: Number,
        optional: false
    },
}));

*/