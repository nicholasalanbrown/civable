//Collection creation and schema declaration for comments

Comments = new Mongo.Collection("comments");

Scema = {};

Comments.attachSchema(new SimpleSchema({
    postId: {
        type: String,
        optional: false
    },
    commenterId: {
        type: String,
        optional: false
    },
    commenterName: {
        type: String,
        optional: false
    },
    commenterPicture: {
        type: String,
        optional: false
    },
    text: {
        type: String,
        optional: false
    },
    createdAt: {
        type: String,
        optional: false
    },
    upvotes: {
        type: Number,
        optional: true
    }
}));
