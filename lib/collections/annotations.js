//Collection creation and schema declaration for annotations

Annotations = new Mongo.Collection("annotations");

Scema = {};

Annotations.attachSchema(new SimpleSchema({
    postId: {
        type: String,
        optional: false
    },
    nodeId: {
        type: String,
        optional: false
    },
    annotatorId: {
        type: String,
        optional: false
    },
    annotatorName: {
        type: String,
        optional: false
    },
    annotatorPicture: {
        type: String,
        optional: false
    },
    text: {
        type: String,
        optional: false
    },
    createdAt: {
        type: String,
        optional: false
    },
    upvotes: {
        type: Number,
        optional: true
    }
}));
