//THis is where all routes are configured.
//TODO: Add FlowRouter SSR package and move components to lib for SEO

//I'm configuring this route to check for login, but a recent Meteor Devshop talk I saw suggests this is no thte right approach. I think instead it should be done with redirects at the component level in React

var appRoutes = FlowRouter.group({
    name: 'appRoutes',
    triggersEnter: [function(context, redirect) {
        if (!Meteor.userId() & !Meteor.loggingIn()) {
            redirect('login');
        }
    }]
});

var publicRoutes = FlowRouter.group({
    name: 'publicRoutes'
});

FlowRouter.route("/", {
    name: 'home',
    action(params) {
        renderMainLayoutWith(<C.FeedContainer />);
    }
});

publicRoutes.route("/post/:slug", {
    name: 'singlePostbySlug',
    action(params) {
        renderMainLayoutWith(<C.PostContainer postSlug={params.slug} />);
    }
});

publicRoutes.route("/post/id/:_id", {
    name: 'singlePostbyId',
    action(params) {
        renderMainLayoutWith(<C.PostContainer postId={params._id} />);
    }
});

publicRoutes.route("/post/id/:_id/edit", {
    name: 'editPost',
    action(params) {
        renderMainLayoutWith(<C.PostEditForm postId={params._id} />);
    }
});



appRoutes.route("/create", {
    name: 'create',
    action(params) {
        renderMainLayoutWith(<C.PostCreateForm />);
    }
});

FlowRouter.route("/myposts", {
    name: 'myPosts',
    action(params) {
        renderMainLayoutWith(<C.UserPostContainer />);
    }
});

FlowRouter.route("/login", {
    name: "login",
    action(params) {
        renderMainLayoutWith(<C.UserLogin />);
    }
});

FlowRouter.route("/signup", {
    name: "signup",
    action(params) {
        renderMainLayoutWith(<C.UserLogin />);
    }
});

//RIght now there is only one layout, configured with the ReactLayout package. We may need more for routes that do not have a navbar, footer, etc, and I'm still not clear how this jives with login checks at the component level.

function renderMainLayoutWith(component) {
    ReactLayout.render(C.MainLayout, {
        content: component,
        footer: <C.MainFooter />
    });
}
